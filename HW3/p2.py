#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 16:16:15 2019

@author: mark
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 09:08:27 2019

@author: mark
"""

import numpy as np
import math
import random
from scipy.linalg import svd
from sklearn import metrics
from datetime import datetime

def data_generation(theta,n=100,k=50,d=50):
    
    data=np.zeros(shape=(k*n,d+k), dtype=np.float)
    for i in range(k*n):
        data[i][k:] = np.random.normal(0, theta, (1,d))
        for j in range(k):
            if i%k == j:
                data[i][j] = 1
            else:
                data[i][j] = 0
    return data

# This is not strictly PCA algorithm.
# Based on my understanding, our purpose here is to reduce the noise instead of dimension reduction.
def SVD(m, data):    
    #Normalization
    n1,n2 = data.shape
    data = data - np.sum(data,axis=0)/n1
    assert np.allclose(np.sum(data), np.zeros(n2))
    
    u,s,vh = svd(data, full_matrices=False)
    
    # I only preserve largerst m singular value information
    smat = np.diag(s[:m])
    
    projected = np.dot(u[:,:m],smat)
    
    return projected

def distance(a,b):
    return np.sum((a-b)**2)

def extract_initial_cluster(k,data):
    n,dim = data.shape
    tStart = datetime.now()
    print('Select the initial cluster...')

    # Select the first cluster uniform randomly
    c = data[random.randint(0,n-1)][:]
    C = np.array([c])
    
    for i in range(k-1):
        distance_map = {}
        for j in range(n):
            # Get the distance from this data to each center
            tmp_data = np.sum((C-data[j])**2,axis=1)
            # pick the minimum one to get its Probabilistic
            center = np.argmin(tmp_data)
            distance_map[j] = tmp_data[center]
        
        # Calculate the distribution
        summation = sum(distance_map.values())        
        probability = []
        for key in sorted(distance_map.keys()):
            probability.append(distance_map[key]/summation)
        
        # Given distribution, pick the next cluster center
        c_index = np.random.choice(n,p=probability)
        # Add the new picked cluster center
        C = np.vstack((C,data[c_index]))
    tEnd = datetime.now()
    print('extract_initial_cluster', tEnd-tStart)
    return C

def KMeans(data,k=3):
    #k=50
    n,dim = data.shape
    
    # Select initial cluster
    C = extract_initial_cluster(k,data)
    #    C = Initial_cluter[:]
    
    assignment,flag = get_assignment(C, list(range(n)), data)
    
    # Update the cluster center
    for c, points in assignment.items():
        C[c] = np.sum(data[points],axis=0)/len(points)

    count = 0
    while flag:
        count += 1
        flag = False
        new_assignment={}
        
         
        for c, points in assignment.items():
            # Becasue the input data are in the same cluster, if tmp_assignment has more than one cluster,
            # the original assignment changes, we need to do the next iteration
            tmp_assignment,tmp_flag = get_assignment(C, points, data)
             # if the assignment of each cluster doesn't change, we stop the iteration
            flag = flag or tmp_flag
            
            # Collect assignments
            for tmp_c, tmp_points in tmp_assignment.items():
                if tmp_c not in new_assignment:
                    new_assignment[tmp_c] = []
                new_assignment[tmp_c] = new_assignment[tmp_c] + tmp_points
           
        
        # Update the cluster center
        for c, points in new_assignment.items():
            C[c] = np.sum(data[points],axis=0)/len(points)
        assignment = new_assignment
        
    print('iteration num', count)
    return assignment,C

def get_assignment(C, index, data):
    '''
    flag: indicates whether the algorithm converges or not
    '''
    C_tmp = np.array(C)
    assignment={}
    if not index or len(index)==0:
        return assignment, False
    
    # For each data point, I assign it to its clostest cluster center.
    for i in index:
        tmp_data = np.sum((C_tmp-data[i])**2,axis=1)
        center = np.argmin(tmp_data)
        if center not in assignment:
            assignment[center] = []

        assignment[center].append(i)
    flag = len(assignment.keys()) > 1
    

    return assignment, flag

def evaluate(predict, gound_truth):
    print("Homogeneity: %0.3f" % metrics.homogeneity_score(gound_truth, predict))

n_parameter = [100,200]
k_parameter = [50,100]
for n in n_parameter:
    for k in k_parameter:
        gound_truth = [i for i in range(k)]*n
        d_parameter = [k, 2*k]
        theta_parameter = [1/k, 1/math.sqrt(k), 0.5]
        for d in d_parameter:
            for theta in theta_parameter:
                
                print('------------------------------------------')
                print(n,k,d,theta)
                tStart = datetime.now()
                original_data = data_generation(theta,n,k,d)
                data = SVD(k,original_data)
                svdEnd = datetime.now()
                print('SVD',svdEnd-tStart)
                assign,C = KMeans(data,k)
                tEnd = datetime.now()
                print('Kmeans++', tEnd-tStart)
                predict = [0 for i in range(n*k)]
                
                for key,val in assign.items():
                    for i in val:
                        predict[i] = key
                evaluate(predict,gound_truth)