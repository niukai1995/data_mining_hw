#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 23:46:18 2019

@author: mark
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 15:57:13 2019

@author: mark
"""
import hashlib 
from random import seed
from random import randint
import pickle
import pandas as pd
import sys

with open('Corpus.data', 'rb') as config_dictionary_file:
    Corpus = pickle.load(config_dictionary_file)
with open('link_dict.data', 'rb') as config_dictionary_file:
    link_dict = pickle.load(config_dictionary_file)

df = {} # token_id ——> frequent in the Corpus
tf = {} # link_id ——> {tokeni, fi}
link_dict = {} # link address ——> id
token_to_id = {} # token ——> id
for index,row in Corpus.iterrows():
    link_address = row['url']
    content = row['short_description']
    
    link_id = None
    if link_address not in link_dict:
        link_id = len(link_dict)
        link_dict[link_address] = link_id
    else:
        link_id = link_dict[link_address]
    
    # count for tf
    token_dict = {}
    for token in content:
        if token in token_to_id:
            token_id = token_to_id[token]
        else:
            token_id = len(token_to_id)
            token_to_id[token] = token_id

        if token_id in token_dict:
            continue
            token_dict[token_id] += 1
        else:
            token_dict[token_id] = 1
    
    tf[link_id] = token_dict

def add_neighbor(i,j,near_docu_test):
    if i in near_docu_test:
        near_docu_test[i].append(j)
    else:
        near_docu_test[i] = [j]
    if i != j:
        if j in near_docu_test:
            near_docu_test[j].append(i)
        else:
            near_docu_test[j] = [i]
    
near_docu_test = {}
keys = tf.keys()
theta = 0.8
for i in keys:
    for j in keys:
        post1 = tf[i]
        post2 = tf[j]
        n1 = len(set(post1).intersection(post2))
        n2 = len(set(post1).union(post2))
           
        if n1/n2 > theta:
            add_neighbor(i,j,near_docu_test)
for i,j in near_docu_test.items():
    near_docu_test[i] = list(set(j))

with open('spark_lsh_result.data', 'rb') as config_dictionary_file:
    lsh_result_tmp_1 = pickle.load(config_dictionary_file)
lsh_result_tmp = {}
for i in lsh_result_tmp_1:
    lsh_result_tmp[i[0]] = i[1]
#    print(i[0],i[1])
tp=0
tn=0
fn=0
fp=0
for i in keys:
    if i not in lsh_result_tmp:
        continue
    list1 = near_docu_test[i]
    list2 = list(lsh_result_tmp[i])
    tp_tmp = len(set(list1).intersection(list2))
    print(tp_tmp)
    fn_tmp = len(list1)-1 - tp_tmp
    fp_tmp = len(list2) - tp_tmp
    tn_tmp = len(keys) - fn_tmp - fp_tmp - tp_tmp
    tp += tp_tmp
    fp += fp_tmp
    fn += fn_tmp
    tn += tn_tmp
r = tp/(tp+fn)
p = tp/(tp+fp)
print(r,p)