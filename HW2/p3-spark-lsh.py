
import hashlib 
from random import seed
from random import randint
import pickle
import pandas as pd
import sys

from pyspark.sql import SQLContext
try:
    from StringIO import StringIO ## for Python 2
except ImportError:
    from io import StringIO ## for Python 3
from pyspark.sql import DataFrameReader
from pyspark.sql import SparkSession

# seed random number generator
seed(1)
# generate some integers
random_nums = [randint(0,1000) for i in range(100)]
def hashFamily(i):
    resultSize = 8 # how many bytes we want back 
    maxLen = 20 # how long can our i be (in decimal) 
    salt = str(i).zfill(maxLen)[-maxLen:] 
    def hashMember(x):
        return hashlib.sha1((x + salt).encode('utf-8')).digest()[-resultSize:] 
    return hashMember

hashes = [hashFamily(i) for i in random_nums]
#sc = SparkContext( 'local')
logFile = "/Users/mark/SAPIENZA/Session3/Data Mining/HW2019/HW2/collect_data.tsv"

from pyspark import SparkContext
from pyspark.sql import Row
#sc = SparkContext()
sqlContext = SQLContext(sc)
spark = SparkSession.builder \
     .master("local") \
     .appName("Word Count") \
     .config("spark.some.config.option", "some-value") \
     .getOrCreate()


train = spark.read.csv(path = logFile, header = True,sep='\t',multiLine=True)

data = train.select(train['url\r'],train['short_description']).rdd

def check(h):
    return h[0] and h[1]
b=16
#h=10

def shingles_family(k=10,hash_funcs=[lambda x:x]):
    def shingles(data):
        '''
        url: a string
        doc: a string
        hash_func: set a default hash_func as map itself
        Output: a list of (url,shingle_id)
        '''
        url, doc = data[0],data[1]
        result = []
        
        #Here I choose to eliminate the white space between the tokens
        doc = ''.join(doc.split(' '))
#        print(doc)
        for it, hash_func in enumerate(hash_funcs):
            for i in range(k, len(doc)+1):
                result.append(((it,url),int.from_bytes(hash_func(doc[i-k:i]), byteorder='big')))
        return result
    return shingles
#
#.flatMap(lambda h: [((it,h[0]),int.from_bytes(hash(str(i)), byteorder='big')) for i in h[1].split(' ') for it,hash in enumerate(hashes)])\
#

with open('link_dict.data', 'rb') as config_dictionary_file:
    link_dict = pickle.load(config_dictionary_file)
    
minhash_result = data.filter(check)\
.flatMap(shingles_family(10,hashes))\
.reduceByKey(lambda a,b : a if a < b else b)

# group some row into one brand
# collect the rows in the same brand
brands = minhash_result.map(lambda h: ((h[0][0] % b, link_dict[h[0][1].strip('\r')]),str(h[0][0])+'_'+str(h[1]))).groupByKey() 

# I should keep the result order of each hash function
# I organize the signature in such a way “str(h[0][0])+'_'+str(h[1])”, so I can use sort to keep the order of hash function consistently
#((brand_id, hash_value), [document_id])
hash_value = brands.map(lambda h: (h[0], sorted(list(h[1])))).map(lambda h: ((h[0][0],hashes[0](str(h[1]))),[h[0][1]]))

# collect all the documents who have the same hash_value #(((brand_id, hash_value), [document_id1,...,document_idn]))
collect_similar_document = hash_value.reduceByKey(lambda a,b: a+b)

# generate pairs in [document_id1,...,document_idn]
# filter the empty one
#(docu_id,[similar1,similar2....similarn]) 
# delete the repetitions
lsh_result = collect_similar_document.flatMap(lambda h: [(i,j) for i in h[1] for j in h[1] if i != j])\
.filter(lambda x: x)\
.groupByKey()\
.map(lambda h: (h[0], set(h[1]))) 

print(lsh_result.take(3))
with open('spark_lsh_result.data', 'wb') as config_dictionary_file:
        pickle.dump(lsh_result, config_dictionary_file)
