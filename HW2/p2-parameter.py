#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 16:27:06 2019

@author: mark
"""

from scipy import integrate
import sys
import pickle
import matplotlib.pyplot as plt

with open('Corpus.data', 'rb') as config_dictionary_file:
    Corpus = pickle.load(config_dictionary_file)
    
m = 200
theta = 0.8
for r in range(1,30):
    ans = sys.maxsize
    b = m // r
    
    def f1(x):
        return (1-x**r)**b
    false_negative, err = integrate.quad(f1, theta, 1)

    def f2(x):
        return 1-(1-x**r)**b
    false_positive, err = integrate.quad(f2, 0, theta)
    
    true_positive, err = integrate.quad(f2, theta, 1)
    
    precision = true_positive/(true_positive + false_positive)
    recall = true_positive/(true_positive + false_negative)
    x = precision > 0.98
#    y = v2 <= 0.1
    if x:
        print('potential parameters and corresponding error: \n','r',r,'b',b,'precision',precision,'recall',recall)

#Plot
#-----------------------------------------------------------------
r = 22
b = 9
def f2(x):
        return 1-(1-x**r)**b
def f1(x):
        return (1-x**r)**b
x = [i*0.01 for i in range(100)]
y = [f2(i) for i in x]
plt.vlines(0.8, 0, 1, linestyle="dashed")
plt.hlines(1, 0, 1, linestyle="dashed")
plt.hlines(f2(0.8), 0, 0.8, linestyle="dashed")
plt.scatter(x, y, zorder=2)
plt.plot(x, y)
plt.plot(x=0.8)

v1, err = integrate.quad(f1, theta, 1)
v2, err = integrate.quad(f2, 0, theta)
print(v1,v2)