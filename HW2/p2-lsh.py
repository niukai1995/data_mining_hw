#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  3 15:04:46 2019

@author: Kai Niu

"""
'''
    1.shingling
    2.minwise hashing
    3.locality-sensitive hashing
'''

# Step 1
#-------------------------------------------------------------------------------------------------------------------
import hashlib 
from random import seed
from random import randint
import pickle
import pandas as pd
import sys

# seed random number generator
seed(1)
# generate some integers
random_nums = [randint(0,1000) for i in range(100)]
    
def hashFamily(i):
    resultSize = 8 # how many bytes we want back 
    maxLen = 20 # how long can our i be (in decimal) 
    salt = str(i).zfill(maxLen)[-maxLen:] 
    def hashMember(x):
        return hashlib.sha1((x + salt).encode('utf-8')).digest()[-resultSize:] 
    return hashMember

hashes = [hashFamily(i) for i in random_nums]
shingle_to_id = {}
inverted_dictionary = {}
k = 10

with open('Corpus.data', 'rb') as config_dictionary_file:
    Corpus = pickle.load(config_dictionary_file)
with open('link_dict.data', 'rb') as config_dictionary_file:
    link_dict = pickle.load(config_dictionary_file)


def shingles_family(k=10,hash_funcs=[lambda x:x]):
    def shingles(data):
        '''
        url: a string
        doc: a string
        hash_func: set a default hash_func as map itself
        Output: a list of (url,shingle_id)
        '''
        url, doc = data[0],data[1]
        result = []
        
        #Here I choose to eliminate the white space between the tokens
        doc = ''.join(doc.split(' '))
#        print(doc)
        for it, hash_func in enumerate(hash_funcs):
            for i in range(k, len(doc)+1):
                result.append(((it,url),int.from_bytes(hash_func(doc[i-k:i]), byteorder='big')))
        return result
    return shingles

inverted_dict = {}
for url,doc in zip(Corpus['url'],Corpus['short_description']):
    doc_id = link_dict[url]
    doc = ''.join(doc)
    
    data_set = []
    for i in range(k, len(doc)+1):
            data_set.append(doc[i-k:i])
    inverted_dict[doc_id] = set(data_set)

# Step 2
#-------------------------------------------------------------------------------------------------------------------
min_hash_data = pd.DataFrame()
for round,hash_func in enumerate(hashes):                                       #Generate K signature for each document
    round_data = {}
    
    for doc_id in link_dict.values():
        min_data = sys.maxsize
        for i in inverted_dict[doc_id]:
            value = int.from_bytes(hash_func(str(i)), "big")                    #Calculate the hash value for each token in the document
            if value < min_data:
                min_data = int.from_bytes(hash_func(str(i)), "big")             #Select the minimum value as signature of the document
                hash_value = i
        round_data[doc_id] = hash_value
    min_hash_data[round] = pd.Series(round_data)

# Step 3 
#-------------------------------------------------------------------------------------------------------------------
fash_func = hashes[-1]
# Be careful about this operation
min_hash_data = min_hash_data.T
M = len(min_hash_data)
b = 16
r = 6
result = {}
for i in range(b):
    data = min_hash_data[i*r:(i+1)*r]
    tmp_dict = {}
    for file_id in range(data.shape[1]):
        key = str(data[file_id].values)                                         #Concatenate the signature in the same brand as the hash function input
        value = fash_func(key)                                                  #Collect the document who has the same hash value
        if value not in tmp_dict:                               
            tmp_dict[value] = []
        tmp_dict[value].append(file_id)
    
    # Collect the result
    for key,near_sets in tmp_dict.items():
        for ele in near_sets:
            if ele in result:
                result[ele] = list(set(result[ele] + near_sets))
            else:
                result[ele] = near_sets
                
with open('lsh_result.data', 'wb') as config_dictionary_file:
  pickle.dump(result, config_dictionary_file)