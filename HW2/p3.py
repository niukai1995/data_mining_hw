#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  3 16:41:38 2019

@author: mark
"""

# Step 1
#-------------------------------------------------------------------------------------------------------------------
import hashlib 
from random import seed
from random import randint
import pickle
import pandas as pd
import sys

# seed random number generator
seed(1)
# generate some integers
random_nums = [randint(0,1000) for i in range(100)]
    
def hashFamily(i):
    resultSize = 8 # how many bytes we want back 
    maxLen = 20 # how long can our i be (in decimal) 
    salt = str(i).zfill(maxLen)[-maxLen:] 
    def hashMember(x):
        return hashlib.sha1((x + salt).encode('utf-8')).digest()[-resultSize:] 
    return hashMember

hashes = [hashFamily(i) for i in random_nums]
shingle_to_id = {}
inverted_dictionary = {}
documents = [['hello' ,'world', 'word', 'my', 'gad']]
k = 2

with open('Corpus.data', 'rb') as config_dictionary_file:
    Corpus = pickle.load(config_dictionary_file)
with open('link_dict.data', 'rb') as config_dictionary_file:
    link_dict = pickle.load(config_dictionary_file)

inverted_dict = {}
for url,doc in zip(Corpus['url'],Corpus['short_description']):
    doc_id = link_dict[url]
    
    data_set = []
    for i in range(k-1,len(doc)):
        shingle = doc[i-1] + '_' + doc[i]
        shingle_id = None
        if shingle in shingle_to_id:
            shingle_id =  shingle_to_id[shingle]
        else:
            shingle_id = len(shingle_to_id)
            shingle_to_id[shingle] = shingle_id
        data_set.append(shingle_id)
    inverted_dict[doc_id] = set(data_set)

def shingles_family(k=10,hash_funcs=[lambda x:x]):
    def shingles(data):
        '''
        url: a string
        doc: a string
        hash_func: set a default hash_func as map itself
        Output: a list of (url,shingle_id)
        '''
        url, doc = data[0],data[1]
        result = []
        
        #Here I choose to eliminate the white space between the tokens
        doc = ''.join(doc.split(' '))
#        print(doc)
        for it, hash_func in enumerate(hash_funcs):
            for i in range(k, len(doc)+1):
                print(doc[i-k:i])
                result.append(((it,url),int.from_bytes(hash_func(doc[i-k:i]), byteorder='big')))
        return result
    return shingles

#Step 2 Generate signature
