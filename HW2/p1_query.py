#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 16 22:05:25 2019

@author: mark
"""
import pickle
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer 
from p1_build_index import preprocessing
from p1_build_index import tf_generator
import math

# find the most related announcement       
#-------------------------------------------------------------------------------------------------------------------
def query(q,invert_index):
    # split and clean the token
#    tokens = q.split(' ')
#    tokens = [i.strip() for i in tokens]
    
    N = len(invert_index)
    
    potential_result = {}
    for token_id, tf in tf_generator(q, token_to_id).items():
        
        # Note that we need to prevent new words in query
        if token_id not in invert_index:
            continue
        
	# calculate the tf-idf for token_id
        df = len(invert_index[token_id])
        idf = math.log(N/df)
        tf_idf = idf * tf

	# sum the score for each potential document
        for post in invert_index[token_id]:
            docId,score=post[0],post[1]
            if docId not in potential_result:
                potential_result[docId] = 0
            potential_result[docId] += tf_idf * score

    #pick the document with highest score
    ans = None
    ans_val = -1
    for k,val in potential_result.items():
        if val > ans_val:
            ans = k
            ans_val = val
    print('best doc score', ans_val)
    return ans

if __name__== "__main__":
    print('loading data.......')
    with open('idf.data', 'rb') as config_dictionary_file:
        idf = pickle.load(config_dictionary_file)
    with open('token_to_id.data', 'rb') as config_dictionary_file:
        token_to_id = pickle.load(config_dictionary_file)
    with open('invert_index.data', 'rb') as config_dictionary_file:
        invert_index = pickle.load(config_dictionary_file)
    with open('Corpus.data', 'rb') as config_dictionary_file:
        Corpus = pickle.load(config_dictionary_file)
    with open('link_dict.data', 'rb') as config_dictionary_file:
        link_dict = pickle.load(config_dictionary_file)
    
    id_to_url={}
    for index,val in link_dict.items(): 
        id_to_url[val] = index
    print('finish loading')

    while True:
        q = input('Pleas input your query: ')
        q = preprocessing(q)
        ans = query(q,invert_index)
        print('DocId', ans)
        print('Url', id_to_url[ans])
        
    
    
