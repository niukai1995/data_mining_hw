#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  3 14:47:50 2019

@author: mark
"""

import pandas as pd
import re
from sklearn.utils import shuffle
import math
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer 
import pickle


def lemmatize_data(input_data):
    # input is a list of string
    result = []
    wnl = WordNetLemmatizer()
    for token in input_data:
        result.append(wnl.lemmatize(token))
    return result 


def clean_context(ctx_in, has_target=False):
            replace_newline = re.compile("\n")
            replace_dot = re.compile("\.")
            replace_cite = re.compile("'")
            replace_frac = re.compile("[\d]*frac[\d]+")
            replace_num = re.compile("\s\d+\s")
            rm_context_tag = re.compile('<.{0,1}context>')
            rm_cit_tag = re.compile('\[[eb]quo\]')
            rm_misc = re.compile("[\[\]\$`()%/,\.:;-]")

            ctx = replace_newline.sub(' ', ctx_in)  # (' <eop> ', ctx)

            ctx = replace_dot.sub(' ', ctx)  # .sub(' <eos> ', ctx)
            ctx = replace_cite.sub(' ', ctx)  # .sub(' <cite> ', ctx)
            ctx = replace_frac.sub(' <frac> ', ctx)
            ctx = replace_num.sub(' <number> ', ctx)
            ctx = rm_cit_tag.sub(' ', ctx)
            ctx = rm_context_tag.sub(' ', ctx)
            ctx = rm_misc.sub(' ', ctx)

            word_list = [word for word in re.split('`|, | +|\? |! |: |; |\(|\)|_|,|\.|"|“|”|\'|\'', ctx.lower()) if word]
            return word_list

def preprocessing(line):
    '''
    input: a string
    output: a list [token1, token2, ... token_n]
    '''
    stop_words = set(stopwords.words('italian'))
    return [token for token in lemmatize_data(clean_context(line)) if token not in stop_words]

def tf_generator(content,token_to_id={}):
    # count for tf
    token_dict = {}
    for token in content:
        if token in token_to_id:
            token_id = token_to_id[token]
        else:
            token_id = len(token_to_id)
            token_to_id[token] = token_id

        if token_id in token_dict:
#            continue
            token_dict[token_id] += 1
        else:
            token_dict[token_id] = 1
            
    for k,v in token_dict.items():
        token_dict[k] = (1 + math.log(v))
    return token_dict
    
if __name__== "__main__":
    ## load the dataset
    data_frame = pd.read_csv('collect_data.tsv', sep='\t', header=0)
    
    print('output the statistics information')
    data_frame.info()
    
    #-------------------------------------------------------------------------------------------------------------------
    # preprocessing the data
    Corpus = shuffle(data_frame)
    
    
    
    # Remove blank rows if any.
    Corpus['short_description'].dropna(inplace=True)
    Corpus['short_description'] = [preprocessing(entry) for entry in Corpus['short_description']]
    
    #-------------------------------------------------------------------------------------------------------------------
    # Build tf-idf function
    ''' 
        In WIR, we assign each document a docuID. Here we assign each link a linkID. 
        And we treat the linkID as the query result
    '''
    
    df = {} # token_id ——> frequent in the Corpus
    tf = {} # link_id ——> {tokeni, fi}
    link_dict = {} # link address ——> id
    token_to_id = {} # token ——> id
    
    
    
    # Note row is a series object here
    for index,row in Corpus.iterrows():
        link_address = row['url']
        content = row['short_description']
        
        link_id = None
        if link_address not in link_dict:
            link_id = len(link_dict)
            link_dict[link_address] = link_id
        else:
            link_id = link_dict[link_address]
        
        
        token_dict = tf_generator(content,token_to_id)

        # I am thinking whether I can skip this step, using the len(invert_index[token]) to represent the df
        # I can't do it because I can't normalize it with idf information before constructing inverted_index.
        # count for df
        content_set = set(content)
        for token in content_set:
            token_id = token_to_id[token]
            if token_id in df:
                df[token_id] += 1
            else:
                df[token_id] = 1
                
        tf[link_id] = token_dict
    
    N = len(Corpus)
    for key,value in df.items():
        df[key] = math.log(N/value)
    
    # Build invert index
    #-------------------------------------------------------------------------------------------------------------------
    
    invert_index = {} # token ——> [(link_id,tf-idf socre)]
    for key,value in tf.items():
        normalization_sum = 0
        for k,v in value.items():
            normalization_sum += v * df[k]
        
        for k,v in value.items():
            if k in invert_index:
                invert_index[k].append((key,((1 + math.log(v)) * df[k])/normalization_sum))
            else:
                invert_index[k] = [(key,((1 + math.log(v)) * df[k])/normalization_sum)]
    
    
    
    # save the data into the disk         
    #-------------------------------------------------------------------------------------------------------------------
    '''
    Corpus
    invert_index
    link_dict
    token_to_id
    df(idf)
    '''
    with open('token_to_id.data', 'wb') as config_dictionary_file:
        pickle.dump(token_to_id, config_dictionary_file)
    
    with open('idf.data', 'wb') as config_dictionary_file:
        pickle.dump(df, config_dictionary_file)
    
    with open('Corpus.data', 'wb') as config_dictionary_file:
        pickle.dump(Corpus, config_dictionary_file)
    
    with open('invert_index.data', 'wb') as config_dictionary_file:
        pickle.dump(invert_index, config_dictionary_file)
    
    with open('link_dict.data', 'wb') as config_dictionary_file:
        pickle.dump(link_dict, config_dictionary_file)
    
    
    
    
    
    
