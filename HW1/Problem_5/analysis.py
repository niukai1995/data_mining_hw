#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 08:41:55 2019

@author: Kai Niu
"""
import pandas as pd
# Analysis the dataset
data_frame = pd.read_csv('/Users/mark/SAPIENZA/Session3/Data Mining/HW2019/HW1/lastfm-dataset-1K/collect_data.tsv', sep='\t', header=0)

print('output the statistics information')
data_frame.info()

print('--------------------------')
# There are 1459 announcements

# delete the announcements whose price is None
filtered_data = data_frame[data_frame['price'] != 'None']
filtered_data.info()

print('--------------------------')


# calculate the average apartment price for the filtered dataset
price = filtered_data.price.astype('int64')
print('The average apartment price: ', price.mean())