#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 15:59:14 2019

@author: Kai Niu
"""

# Import libraries
import requests
import urllib.request
import time
from bs4 import BeautifulSoup
import csv

# Set the URL you want to webscrape from
# observe the rule of this url 
url_part1 = 'https://www.kijiji.it/case/affitto/annunci-roma/appartamento/?p='

# I want to extract the number of pages for a certain search query
init_url = url_part1 + str(1)
response = requests.get(init_url)
soup = BeautifulSoup(response.text, "html.parser")
mydivs = soup.findAll("h4", {"class": "pagination-hed"})
nums = int(mydivs[0].string.split(' ')[-1])

# collect all the target urls and store them in filter_result
filter_result = []
for num in range(1,nums):
    url = url_part1 + str(num)
    response = requests.get(url)
    
    # Parse HTML and save to BeautifulSoup object¶
    soup = BeautifulSoup(response.text, "html.parser")
        
    result = soup.find_all('li')
    
    
    for i in result:
        if 'class' in i.attrs:
            # attr is a list containing all the classes for a certain li tag
            attr = i.get('class')
            if 'gtm-search-result' in attr :
                if 'data-href' in i.attrs:
                    filter_result.append(i.get('data-href'))

print("finish extracting links")
# -----------------------------------------------------------------------------------------------------------------
# Given a list of target URL, extract information data from each html file

with open('./collect_data.tsv','wt') as out_file:
    tsv_writer = csv.writer(out_file, delimiter='\t')
    tsv_writer.writerow(['title', 'short_description','location','price','timestamp','url'])
    data = []
    for url in filter_result:
        response = requests.get(url)
        # Parse HTML and save to BeautifulSoup object¶
        soup = BeautifulSoup(response.text, "html.parser")
        
        box_content = soup.find("div", {"class":"box__content"})
        if not box_content:
            print(url)
            continue
        #    print(box_content)
        view_num, date = box_content.find_all("span", {"class":"vip__informations__value"})
        date_val = date.string
        date_val = date_val.strip()
        
        title = box_content.find("h1",{"class":"vip__title"})
        title_val = title.string.strip()
        
        price = box_content.find("h2",{"class":"vip__price"})
        if  price:
            price_val = price.string.strip().split(' ')[0]
            tmp = 0
            if ',' in price_val:
                eles = price_val.split(',')
                tmp += int(eles[0])*1000
                tmp += int(eles[1])
                price_val = tmp
            elif '.' in price_val:
                eles = price_val.split('.')
                tmp += int(eles[0])*1000
                tmp += int(eles[1])
                price_val = tmp
            else:
                price_val = int(price_val)
        else:
            price_val = "None"
        
        description = box_content.find("p",{"class":"vip__text-description"})
        if description:
            description_val = description.string.strip()
        else:
            description_val = "None"
        
        location = box_content.find("div",{"class":"vip__location"}).find("span")
        if location:
            location_val = location['title'].strip()
        else:
            location_val = "None"
        
        
        data.append((title_val,description_val,location_val,price_val,date_val,url))
        tsv_writer.writerow([title_val,description_val,location_val,str(price_val),date_val,url])
        time.sleep(1)

