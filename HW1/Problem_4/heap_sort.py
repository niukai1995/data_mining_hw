#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 17:29:05 2019

@author: Kai Niu
"""
file_name = 'result.tsv'
f = open(file_name,'r')
data = f.readlines()

# Build the invert index
result = {}
for i in data:
    eles = i.strip().split('\t')
    for i in range(1,len(eles)):
        if eles[i] in result:
            result[eles[i]].append(eles[0])
        else:
            result[eles[i]] = [eles[0]]

def heapify(arr, n, i):
    smallest = i
    l = 2 * i + 1
    r = 2 * i + 2
    
    if l < n and arr[i][1] > arr[l][1]:
        smallest = l
    
    if r < n and arr[smallest][1] > arr[r][1]:
        smallest = r
    
    if smallest != i:
        arr[i],arr[smallest] = arr[smallest],arr[i]
        
        heapify(arr, n, smallest)

def sort_key(val):
    return val[1]

def heap_sort(data_dict, heap_size=10):
    
    # initialize heap
    keys = list(data_dict.keys())
    heap = [(keys[i],len(data_dict[keys[i]])) for i in range(heap_size)]
    
    
    # build a minheap
    for i in range(heap_size - 1,-1,-1):
        heapify(heap, heap_size, i)
    
    for i in range(heap_size, len(keys)):
        if len(data_dict[keys[i]]) > heap[0][1]:
            print(len(data_dict[keys[i]]))
            heap[0] = (keys[i],len(data_dict[keys[i]]))
            heapify(heap, heap_size, 0)
    
    # output the top eles
    heap.sort(key = sort_key,reverse = True)
    return heap

top_result = heap_sort(result)
print(top_result)

def sort_key2(val):
    return len(val[1])
keys = list(result.keys())
heap = [(i, result[i]) for i in keys]
heap.sort(key = sort_key2,reverse = True)